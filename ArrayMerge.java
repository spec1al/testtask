package testINterest;

import java.util.Arrays;

/*
* 2. Задача на написание кода (желательно выложить решение на гитхаб и прикрепить ссылку)
Дано: 2 отсортированных по возрастанию массива с int
Требуется: наиболее оптимально объединить массивы в один,
причем элементы в нём тоже должны быть отсортированы по возрастанию
*
* */
public class ArrayMerge {

    private static int[] arrayMerge(final int[] firstArray, final int[] secondArray) {
        if (firstArray == null || secondArray == null) {
            throw new IllegalStateException("someone array is null");
        }
        final int firstArrayLength = firstArray.length;
        final int secondArrayLength = secondArray.length;
        //исходя из требований создаем всегда новый массив
        if (firstArrayLength == 0) {
            if (secondArrayLength == 0) {
                System.out.println("zero length array");
                return new int[0];
            }
            final int[] resultArray = new int[secondArrayLength];
            System.arraycopy(secondArray, 0, resultArray, 0, secondArrayLength);
            return resultArray;
        }
        if (secondArrayLength == 0) {
            final int[] resultArray = new int[firstArrayLength];
            System.arraycopy(firstArray, 0, resultArray, 0, firstArrayLength);
            return resultArray;
        }
        final int resultArrayLength = firstArrayLength + secondArrayLength;
        final int[] resultArray = new int[resultArrayLength];
        //Можно вынести в функцию
        if (firstArray[0] > secondArray[secondArrayLength - 1]) {
            System.arraycopy(secondArray, 0, resultArray, 0, secondArrayLength);
            System.arraycopy(firstArray, 0, resultArray, secondArrayLength, firstArrayLength);
            System.out.println("optimized");
            return resultArray;
        }
        if (firstArray[firstArrayLength - 1] < secondArray[0]) {
            System.arraycopy(firstArray, 0, resultArray, 0, firstArrayLength);
            System.arraycopy(secondArray, 0, resultArray, firstArrayLength, secondArrayLength);
            System.out.println("optimized");
            return resultArray;
        }
        /*
         Можно убрать весь код что выше(кроме проверки на null, определения и инициализации массива и длинн)
         - алгоритм останется рабочий.
         Исчесзет оптимизация, в случае когда последний элемент одного из массивов больше первого
         элемента другого массива.
         Добавляя к общему времени алгоритма O(n), 5 инструкции if мы получаем, тот же порядок,
         однако засчет скорости работы System.arraycopy имеем значимый прирост.
         Если мы изначально знаем что диапазоны массивов пересекаются - код выше надо закомментировать.
         */
        int i = 0, j = 0, k = 0;
        while (i < firstArrayLength && j < secondArrayLength) {
            if (firstArray[i] <= secondArray[j]) {
                resultArray[k++] = firstArray[i++];
            } else {
                resultArray[k++] = secondArray[j++];
            }
        }
        //можно вынести в фунцию
        if (i == firstArrayLength) {
            System.arraycopy(secondArray, j, resultArray, k, secondArrayLength - j);
            return resultArray;
        }
        if (j == secondArrayLength) {
            System.arraycopy(firstArray, i, resultArray, k, firstArrayLength - i);
            return resultArray;
        }
        return resultArray;

    }

    //Сортировка стандартной библиотекой
    private static int[] defaultRealisation(final int[] firstArray, final int[] secondArray) {
        final int[] resultArray = new int[firstArray.length + secondArray.length];
        System.arraycopy(firstArray, 0, resultArray, 0, firstArray.length);
        System.arraycopy(secondArray, 0, resultArray, firstArray.length, secondArray.length);
        Arrays.parallelSort(resultArray);
        return resultArray;
    }

    //медот теста
    private static void testRealisation(final int[] firstArray, final int[] secondArray) {
        if (!Arrays.equals(defaultRealisation(firstArray, secondArray), arrayMerge(firstArray, secondArray))) {
            throw new IllegalStateException("arrays not equal");
        }
    }

    public static void main(String[] args) {
        testRealisation(new int[]{}, new int[]{});//оба массива пусты
        testRealisation(new int[]{1, 4, 5, 6, 7}, new int[]{});// один из массивов пуст
        testRealisation(new int[]{}, new int[]{2, 2, 3, 4, 9, 10});//один из массивов пуст
        testRealisation(new int[]{1, 4, 5, 6, 7}, new int[]{2, 2, 3, 4, 9, 10}); //разная длинна
        testRealisation(new int[]{2, 2, 3, 4, 9, 10}, new int[]{1, 4, 5, 6, 7}); //разная длинна
        testRealisation(new int[]{1, 2, 3, 4, 9, 10}, new int[]{11, 12, 13, 14, 15}); // оптимизация
        testRealisation(new int[]{11, 12, 13, 14, 15}, new int[]{1, 2, 3, 4, 9, 10}); // оптимизация
        System.out.println("success");
    }
}
