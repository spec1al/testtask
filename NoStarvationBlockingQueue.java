package testINterest;

import java.util.ArrayDeque;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;


/*
* 1. Найти проблемы в коде.
Исправления требуется сопровождать комментариями с описанием причины и выбора исправления
*
* */




//Я бы использовал готовые структуры

/**
 * @see LinkedBlockingDeque
 */

//Дальнейший текст ввиду того что написано выше, можно не читать

//клас не был объявлен как типизированный
//Объявили класс как Generic, чтобы реализация могла хранить любые типы объектов
public class NoStarvationBlockingQueue<E> {
    private final Lock lock = new ReentrantLock();
    //реализацию лучше сделать на 2 Condition, прямо как в документации
    /**
     * @see Condition
     */
    private final Condition condition = lock.newCondition();

    private final static int MAX_SIZE = 100;
    //Очередь не типизирована, типизируем чтобы не приводить объекты
    private final Queue<E> queue = new ArrayDeque<>(MAX_SIZE);

    //модификаторы доступа добавим public, если планировуется использовать вне пакета.
    public void add(E e) {
        lock.lock();
        try {
            if (queue.size() == MAX_SIZE) {
                condition.await();
            }
            queue.add(e);
        } catch (InterruptedException ignored) {
        } finally {
            lock.unlock();
        }
    }

    //модификаторы доступа добавим public, если планировуется использовать вне пакета
    public E poll() {
        lock.lock();
        try {
            if (queue.size() == 0) {
                condition.await();
            }
            //нет signal - значит, после того как выполниться условие для какого либо из потоков для метода
            // add (queue.size() == MAX_SIZE)), метод add использовать
            // смогут только другие потоки, если проскочат условие на размер
            return queue.poll();
        } catch (InterruptedException e) {
            return null;
        } finally {
            lock.unlock();
        }
    }

    //модификаторы доступа добавим public, если планировуется использовать вне пакета
    public E peek() {
        lock.lock();
        try {
            return queue.peek();
        } finally {
            lock.unlock();
        }
    }
}
